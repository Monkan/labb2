//
//  GameGenerator.h
//  QuizGame2
//
//  Created by ITHS on 2016-02-06.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GameGenerator : NSObject

-(NSArray*)randomQuestion;
-(NSString*)correctAnswer;

@end
