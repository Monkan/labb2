//
//  AppDelegate.h
//  QuizGame2
//
//  Created by ITHS on 2016-02-06.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

