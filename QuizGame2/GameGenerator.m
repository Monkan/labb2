//
//  GameGenerator.m
//  QuizGame2
//
//  Created by ITHS on 2016-02-06.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "GameGenerator.h"

@interface GameGenerator()

@property (nonatomic)NSArray *questions;


@end

@implementation GameGenerator

-(instancetype) init
{
    self = [super init];
    if (self) {
        self.questions =@[@[@"What is the main component of glass?", @"sand", @"clay", @"oil", @"water", @"sand"],
                          @[@"How many Oscars did the movie Titanic win?", @"7", @"9", @"11", @"5", @"11"],
                          @[@"What is Sake made from?", @"wheat", @"rice", @"oat", @"corn", @"rice"],
                          @[@"On which street do Bert and Ernie live?", @"Downing St.", @"Rue Morgue", @"Sesame St.", @"La Rambla", @"Sesame St."],
                          @[@"Which is the longest river i Europe?", @"Donau", @"Seine", @"The Themse", @"Wolga", @"Wolga"],
                          @[@"What was Elvis Presley's middle name?", @"Aaron", @"Paul", @"Fred", @"George", @"Aaron"],
                          @[@"After which animal are the Canary Islands named?", @"bird", @"dog", @"turtle", @"fish", @"dog"],
                          @[@"What is the name of the Barcelona football stadium?", @"Anfield", @"San Siro", @"Camp Nou", @"Parc des Princes", @"Camp Nou"],
                          @[@"Which planet is closest to the sun?", @"Mercury", @"Saturn", @"Tellus", @"Jupiter", @"Mercury"],
                          @[@"What year was Google founded?", @"2000", @"1986", @"1998", @"1992", @"1998"]];
    }
    return self;
}


-(NSArray*)randomQuestion{
    
    return self.questions[arc4random() % self.questions.count];
    
}

-(NSString*)correctAnswer{
//    if ([[self.randomQuestion objectAtIndex:1]isEqualToString:[self.randomQuestion objectAtIndex:5]] || [[self.randomQuestion objectAtIndex:2]isEqualToString:[self.randomQuestion objectAtIndex:5]] || [[self.randomQuestion objectAtIndex:3]isEqualToString:[self.randomQuestion objectAtIndex:5]] || [[self.randomQuestion objectAtIndex:4]isEqualToString:[self.randomQuestion objectAtIndex:5]]){
//    
//    }
    return @"";
}


@end
