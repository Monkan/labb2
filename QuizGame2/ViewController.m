//
//  ViewController.m
//  QuizGame2
//
//  Created by ITHS on 2016-02-06.
//  Copyright © 2016 Monika Svensson. All rights reserved.
//

#import "ViewController.h"
#import "GameGenerator.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UILabel *question;
@property (weak, nonatomic) IBOutlet UILabel *answer;
@property (weak, nonatomic) IBOutlet UIButton *option1;
@property (weak, nonatomic) IBOutlet UIButton *option2;
@property (weak, nonatomic) IBOutlet UIButton *option3;
@property (weak, nonatomic) IBOutlet UIButton *option4;
@property (strong, nonatomic)GameGenerator *gameGenerator;
@property (weak, nonatomic) IBOutlet UIButton *nextQuestion;
@property (weak,nonatomic) NSArray *NewQuestion;
@property (weak, nonatomic)NSString *correctanswer;
@property (weak, nonatomic) IBOutlet UILabel *answerArea;

@end

@implementation ViewController

-(GameGenerator*)gameGenerator {
    if (!_gameGenerator) {
        _gameGenerator = [[GameGenerator alloc] init];
    }
    return _gameGenerator;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (IBAction)newQuestion:(id)sender {
    
    self.NewQuestion = [self.gameGenerator randomQuestion];
    
    self.question.text = [self.NewQuestion objectAtIndex:0];
    [self.option1 setTitle:[self.NewQuestion objectAtIndex:1]forState:normal];
    [self.option2 setTitle:[self.NewQuestion objectAtIndex:2]forState:normal];
    [self.option3 setTitle:[self.NewQuestion objectAtIndex:3]forState:normal];
    [self.option4 setTitle:[self.NewQuestion objectAtIndex:4]forState:normal];
    self.answerArea.text = [self.NewQuestion objectAtIndex:5];
    self.answerArea.hidden = YES;
    self.answer.text = @" ";
  
}

- (IBAction)choiceOfButton:(id)sender {
    NSLog(@"%@", [sender currentTitle]);
    NSLog(@"%@", [self.NewQuestion objectAtIndex:5]);
    if ([[sender currentTitle] isEqualToString:[self.NewQuestion objectAtIndex:5]]) {
        self.answer.text = @"Correct answer!";
    } else {
        self.answer.text = @"Wrong answer";
    }
}



@end
